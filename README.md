# Mayfly

Mayfly is like Snapchat for tweets. You set a time limit for your Twitter posts, after which Mayfly will make them self-destruct.

## Getting Started

A full guide on how to install Mayfly can be found at [Mayf.li](http://mayf.li)

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/mayflyapp/mayfly)

## Changelog & License

Mayfly is a modified version of Tweet Lifetime. 

Copyright (c) Christopher Cliff, 'Tweet Lifetime'. All rights reserved.
Copyright (c) Adam Stoner, 'Mayfly'. All rights reserved.

Both are shared under the Artistic Licence 2.0. 

--------

We (Mayfly) are distributing a Modified Version of the Package as Source in accordance with this extract from Artistic License 2.0: 

(4)  You may Distribute your Modified Version as Source (either gratis
or for a Distributor Fee, and with or without a Compiled form of the
Modified Version) provided that you clearly document how it differs
from the Standard Version, including, but not limited to, documenting
any non-standard features, executables, or modules, and provided that
you do at least ONE of the following:

    (a)  make the Modified Version available to the Copyright Holder
    of the Standard Version, under the Original License, so that the
    Copyright Holder may include your modifications in the Standard
    Version.

    (b)  ensure that installation of your Modified Version does not
    prevent the user installing or running the Standard Version. In
    addition, the Modified Version must bear a name that is different
    from the name of the Standard Version.

    (c)  allow anyone who receives a copy of the Modified Version to
    make the Source form of the Modified Version available to others
    under

        (i)  the Original License or

        (ii)  a license that permits the licensee to freely copy,
        modify and redistribute the Modified Version using the same
        licensing terms that apply to the copy that the licensee
        received, and requires that the Source form of the Modified
        Version, and of any works derived from it, be made freely
        available in that license fees are prohibited but Distributor
        Fees are allowed.

Mayfly complies with all three of these demands. 

Mayfly is available under the same license as the original: Artistic Licence 2.0. A copy of the original licence can be found with the Package.

# Listed below are changes in package: 

* Name change in accordance with Heading 4, Section B.
* Removal of SENTRY_DSN from entire package
* Reordering of tokens in app.json to match apps.twitter.com 
* Rewording in app.json
* Addition of index.html to occupy '/'

# Listed below are changes in general:

* Hosted at http://mayf.li

--------

THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS
IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES. THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY YOUR LOCAL
LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR CONTRIBUTOR WILL
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

--------
